//
//  CardListViewUITests.swift
//  CardsUITests
//
//  Created by A-10474 on 14/11/21.
//

import XCTest

class CardListViewUITests: XCTestCase {
    
    let app = XCUIApplication()
    
    override func setUpWithError() throws {
        continueAfterFailure = false
        XCUIApplication().launch()
    }
    
    override func tearDownWithError() throws {}
    
    func test_navigationBar_with_title_exists() {
        
        let navigationBar = app.navigationBars["MoonPig"]
        XCTAssertTrue(navigationBar.exists)
        let navigationBarText = navigationBar.staticTexts["MoonPig"]
        XCTAssertEqual(navigationBarText.label, "MoonPig")
    }
    
    func test_gridView_exists_onAppear() {
        
        let grid = app.scrollViews.children(matching: .other).element(boundBy: 0).children(matching: .other).element
        let gridExists = grid.waitForExistence(timeout: 10)
        XCTAssertTrue(gridExists)
        let gridCell = grid.children(matching: .other).element(boundBy: 0)
        XCTAssertTrue(gridCell.exists)
        gridCell.tap()
        grid.swipeUp()
    }
}
