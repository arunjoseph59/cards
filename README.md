
## README


### Features
This App is for listing the greeting Cards. It wills show the cards whic are available.

### Requirements
iOS 13 or later
Compactable with iPhone

### prerequesties for builing the App
Xcode 12 or later
Simulator or devices with iOS 14 or later
macOS 10.15.4 or later

### Design pattern
The architectural design pattern MVVM

### Used Dependency
SDWebImage 
SDWebImage is used to load the web images. 

Justification:- This library provides an async image downloader with cache support. Trying to create an image downloader functionality with cache support is time consuming and little bit difficult task. Also this is a proven library with great support and updations. 

### Developer notes
* Interface used is SwiftUI
* Swift 5
* Xcode 12.3 is used to develop and build.
* Included Unit Test and UI Test
* XCTest framework used for Testing.
* API communication is through URLSession
* Codable is used with models for converting json data objects.
* Swift Package manager is used to add dependency.
* API Mock testing is done using local json file (Cards.json).
* LazyVGrid and GridItem is used to build the Cards grid.


### Comments
I am a begginner in SwiftUI. First I had done this task using UIKit (UICollectionView). But I was not satisfied with that. And I have tried to implement the task using SwiftUI. I felt it is quite easy than UIKit. Yeah, I made it using SwiftUI. I am not sure that if it's perfect or not, but I can learn and do more enhancements on this.

If I get more time to spend on this task, definitly I will try to make it perfect. I want to double check my approaches to build UI and communicate with the data and UI. Also I am learning testing dependencies Quick and Nimble, If I get a chance to integrate that in this project I will do.

**I have shared my two approaches to this task. One is using UIKit and the other is this one using SwiftUI. I hope you will check with both to get an idea about my coding.**
