//
//  CardListView.swift
//  Cards
//
//  Created by A-10474 on 13/11/21.
//

import SwiftUI
import SDWebImageSwiftUI

struct CardListView: View {
    
    @StateObject var viewModel: CardsViewModel = CardsViewModel()
    let layout  = [
        GridItem(.flexible(minimum: 150)),
        GridItem(.flexible(minimum: 150))
    ]
    var body: some View {
        NavigationView {
            if !viewModel.cardsDataModel.hasErrorOccured {
                ScrollView( .vertical) {
                    LazyVGrid(columns: layout, content: {
                        ForEach(viewModel.cardsDataModel.cards, id: \.self) { item in
                            CardView(card: item)
                        }.padding()
                    })
                }
                .navigationTitle(Constants.Title.moonPig)
            }
            else{
                Text(viewModel.cardsDataModel.errorMessage)
                    .padding()
                    .navigationTitle(Constants.Title.moonPig)
            }
        }
        .onAppear {
            viewModel.fetchCards()
        }
    }
}

//MARK:- CardsView
struct CardView: View {
    
    var card: Product
    var body: some View {
        ZStack {
            RoundedRectangle(cornerRadius: 15, style: .continuous)
                .fill(Color.white)
                .shadow(radius: 10)
            VStack(
                alignment: .center,
                spacing: 20,
                content: {
                    WebImage(url: URL(string: card.productImage.link.href))
                        .placeholder(Image("placeholder")
                                        .resizable())
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                    Text(card.title)
                        .padding(.bottom, 20)
                })
                .multilineTextAlignment(.center)
                .cornerRadius(15)
        }
        .fixedSize(horizontal: false, vertical: true)
    }
}

//MARK:- preview
struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        CardListView()
    }
}

