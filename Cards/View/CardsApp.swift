//
//  CardsApp.swift
//  Cards
//
//  Created by A-10474 on 13/11/21.
//

import SwiftUI

@main
struct CardsApp: App {
    var body: some Scene {
        WindowGroup {
            CardListView()
        }
    }
}
