//
//  CardsApiClient.swift
//  Cards
//
//  Created by A-10474 on 14/11/21.
//

import Foundation

typealias CardsListCompletion = (Result<Cards, Error>)->Void
/*
 This protocol is for maintaining a separate layer for api calls. Also this will help to mock the api while testing.
*/
protocol CardsApiClient {
    func fetchCards(request: URLRequest, completion: @escaping CardsListCompletion)
}
