//
//  CardsViewModel.swift
//  Cards
//
//  Created by A-10474 on 13/11/21.
//

import SwiftUI

struct CardsDataModel {
    var cards: [Product] = []
    var errorMessage: String = ""
    var hasErrorOccured: Bool = false
}

class CardsViewModel: ObservableObject {
    
    private let networkManager: NetworkManager = NetworkManager()
    @Published var cardsDataModel: CardsDataModel = CardsDataModel()
    
    //MARK:- Fetch Cards
    func fetchCards() {
        guard let url = URL(string: Constants.ApiUrl.apiUrl) else {
            self.cardsDataModel.hasErrorOccured = true
            self.cardsDataModel.errorMessage = CustomError.unknownError.errorDescription ?? "Some error occured"
            return }
        let urlRequest = URLRequest(url: url)
        fetchCards(request: urlRequest) { [weak self] (result) in
            switch result {
            case .success(let cards):
                self?.cardsDataModel.cards = cards.products
            case .failure(let error):
                self?.cardsDataModel.errorMessage = error.localizedDescription
                self?.cardsDataModel.hasErrorOccured = true
            }
        }
    }
}

extension CardsViewModel: CardsApiClient {
    
    func fetchCards(request: URLRequest, completion: @escaping CardsListCompletion) {
        networkManager.request(urlRequest: request, responseType: Cards.self) { (result) in
            DispatchQueue.main.async {
                switch result {
                case .success(let cards):
                    completion(.success(cards))
                case .failure(let error):
                    completion(.failure(error))
                }
            }
        }
    }
}
