//
//  CustomError.swift
//  Cards
//
//  Created by A-10474 on 13/11/21.
//

import Foundation

enum CustomError: Error {
    case unknownError
}

extension CustomError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .unknownError: return Constants.ErrorDescription.unknownError
        }
    }
}
