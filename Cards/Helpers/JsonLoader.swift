//
//  JsonLoader.swift
//  Cards
//
//  Created by A-10474 on 14/11/21.
//

import Foundation

typealias JsonLoaderCompletion<T:Decodable> = (Result<T,Error>)-> Void

class JsonLoader {
    
    static let shared = JsonLoader()
    
    func loadJson<T:Decodable>(filename fileName: String, responseType: T.Type, completion: @escaping (Result<T,Error>)-> Void) {
        if let url = Bundle.main.url(forResource: fileName, withExtension: "json") {
            do {
                let data = try Data(contentsOf: url)
                let decoder = JSONDecoder()
                let jsonData = try decoder.decode(responseType.self, from: data)
                completion(.success(jsonData))
            } catch let error{
                completion(.failure(error))
            }
        }
        else{
            completion(.failure(CustomError.unknownError))
        }
    }
}
