//
//  Constants.swift
//  Cards
//
//  Created by A-10474 on 13/11/21.
//

import Foundation

struct Constants {
    
    struct Placeholders {
        static let placeholder = "placeholder"
    }
    
    struct ErrorDescription {
        static let unknownError = "Something went wrong."
    }
    
    struct ApiUrl {
        static let apiUrl = "https://search.moonpig.com/api/products?size=12&fq=card_shop_id:1&searchFacets=occasion_level_3:occasion%3Ewell%20done%3Enew%20job"
    }
    
    struct Storyboards {
        static let main = "Main"
    }
    
    struct Title {
        static let moonPig = "MoonPig"
    }
}
