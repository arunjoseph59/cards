//
//  Cards.swift
//  Cards
//
//  Created by A-10474 on 13/11/21.
//

import Foundation

struct Cards: Hashable, Codable {
    var products: [Product]
    
    enum CodingKeys: String, CodingKey {
        case products = "Products"
    }
}

//MARK:- Products
 struct Product: Hashable, Codable {
    var price: Price
    var title: String
    var description: String
    var productImage: ProductImage
    var additionalProductImages: [AdditionalProductImages]
    var productVariants: [ProductVariants]
    var facets: [Facets]
    
    enum CodingKeys: String, CodingKey {
        case price = "Price"
        case title = "Title"
        case description = "Description"
        case productImage = "ProductImage"
        case additionalProductImages = "AdditionalProductImages"
        case productVariants = "ProductVariants"
        case facets = "Facets"
    }
}

//MARK:- Price
struct Price: Hashable, Codable {
    var value: Decimal
    var currency: String
    
    enum CodingKeys: String, CodingKey {
        case value = "Value"
        case currency = "Currency"
    }
}

//MARK:- Product image
struct ProductImage: Hashable, Codable {
    var link: Link
    var mimeType: String
    
    enum CodingKeys: String, CodingKey {
        case link = "Link"
        case mimeType = "MimeType"
    }
}

//MARK:- AdditionalProductImages
struct AdditionalProductImages: Hashable, Codable {
    var link: Link
    var mimeType: String
    
    enum CodingKeys: String, CodingKey {
        case link = "Link"
        case mimeType = "MimeType"
    }
}

//MARK:- ProductVariants
struct ProductVariants: Hashable, Codable {
    var id: Int
    var name: String
    var price: Decimal
    var images: [Images]
    
    enum CodingKeys: String, CodingKey {
        case id = "Id"
        case name = "Name"
        case price = "Price"
        case images = "Images"
    }
}

//MARK:- Images
struct Images: Hashable, Codable {
    var link: Link
    var mimeType: String
    
    enum CodingKeys: String, CodingKey {
        case link = "Link"
        case mimeType = "MimeType"
    }
}

//MARK:- Facets
struct Facets: Hashable, Codable {
    var name: String
    var value: String
    
    enum CodingKeys: String, CodingKey {
        case name = "Name"
        case value = "Value"
    }
}

//MARK:- Link
struct Link: Hashable, Codable {
    var href: String
    var method: String
    var title: String
    
    enum CodingKeys: String, CodingKey {
        case href = "Href"
        case method = "Method"
        case title = "Title"
    }
}
