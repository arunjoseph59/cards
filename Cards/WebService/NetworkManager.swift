//
//  NetworkManager.swift
//  Cards
//
//  Created by A-10474 on 13/11/21.
//

import Foundation

class NetworkManager {
    
    func request<T: Decodable>(urlRequest: URLRequest, responseType: T.Type, completion: @escaping (Result<T, Error>)->Void) {
        URLSession.shared.dataTask(with: urlRequest) { data, response, error in
            if let error = error {
                completion(.failure(error))
            }
            else{
                if let data = data {
                    if  let decodedResponse = try? JSONDecoder().decode(responseType.self, from: data) {
                        completion(.success(decodedResponse))
                    }
                    else{
                        completion(.failure(CustomError.unknownError))
                    }
                }
                else{
                    completion(.failure(CustomError.unknownError))
                }
            }
        }.resume()
    }
}
