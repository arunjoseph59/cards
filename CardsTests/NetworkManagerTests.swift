//
//  NetworkManagerTests.swift
//  CardsTests
//
//  Created by A-10474 on 14/11/21.
//

import XCTest
@testable import Cards

class NetworkManagerTests: XCTestCase {
    
    var sut: NetworkManager!
    
    override func setUp() {
        super.setUp()
        sut = NetworkManager()
    }
    
    func test_request_to_server_success() {
        let expectation = self.expectation(description: "Api success")
        guard let url = URL(string: Constants.ApiUrl.apiUrl) else { XCTFail(); return }
        let urlRequest = URLRequest(url: url)
        sut.request(urlRequest: urlRequest, responseType: Cards.self) { (result) in
            switch result {
            case .failure:
                XCTFail()
            case .success:
                expectation.fulfill()
            }
        }
        waitForExpectations(timeout: 40, handler: nil)
    }
    
    func test_request_to_server_fails_with_wrong_url() {
        let expectation = self.expectation(description: "Api Failure")
        guard let url = URL(string: "https://search.moonpig.com/api/products") else { XCTFail(); return }
        let urlRequest = URLRequest(url: url)
        sut.request(urlRequest: urlRequest, responseType: Cards.self) { (result) in
            switch result {
            case .failure:
                expectation.fulfill()
            case .success:
                XCTFail()
            }
        }
        waitForExpectations(timeout: 60, handler: nil)
    }
}
