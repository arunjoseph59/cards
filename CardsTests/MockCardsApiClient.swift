//
//  MockCardsApiClient.swift
//  CardsTests
//
//  Created by A-10474 on 14/11/21.
//

import Foundation
@testable import Cards

class MockCardsApiClient: CardsApiClient {
    
    var hasError: Bool = false
    
    convenience init () {
        self.init(hasError: false)
    }
    
    init(hasError: Bool) {
        self.hasError = hasError
    }
    
    func fetchCards(request: URLRequest, completion: @escaping CardsListCompletion) {
        if let urlString = request.url?.absoluteString {
            if !urlString.isEmpty {
                if hasError {
                    completion(.failure(CustomError.unknownError))
                }
                else{
                    JsonLoader.shared.loadJson(filename: "Cards", responseType: Cards.self) { (result) in
                        switch result {
                        case .failure(let error):
                            completion(.failure(error))
                        case .success(let cards):
                            completion(.success(cards))
                        }
                    }
                }
            }
            else{ completion(.failure(CustomError.unknownError)) }
        }
        else { completion(.failure(CustomError.unknownError)) }
    }
}
