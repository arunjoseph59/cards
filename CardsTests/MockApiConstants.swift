//
//  MockApiConstants.swift
//  CardsTests
//
//  Created by A-10474 on 14/11/21.
//

import Foundation

struct MockApiConstants {
    
    struct ApiUrl {
        static let mockApiUrl = "https://search.moonpig.com/api/products?size=12&fq=card_shop_id:1&searchFacets=occasion_level_3:occasion%3Ewell%20done%3Enew%20job"
        static let wrongMockApiUrl = "https://search.moonpig.com/api/products"
    }
}
