//
//  CardsViewModelTests.swift
//  CardsTests
//
//  Created by A-10474 on 14/11/21.
//

import XCTest
@testable import Cards

class CardsViewModelTests: XCTestCase {
    
    var sut: CardsViewModel!
    
    override func setUp() {
        super.setUp()
        sut = CardsViewModel()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func test_cardsList_api_should_success() {
        let mockApiClient = MockCardsApiClient()
        let expectation = self.expectation(description: "Cards Api will success")
        guard let url = URL(string: MockApiConstants.ApiUrl.mockApiUrl) else { XCTFail(); return }
        let urlRequest = URLRequest(url: url)
        mockApiClient.fetchCards(request: urlRequest) { (result) in
            switch result {
            case .failure:
                XCTFail()
            case .success(let cards):
                XCTAssertNotNil(cards)
                expectation.fulfill()
            }
        }
        waitForExpectations(timeout: 60, handler: nil)
    }
    
    func test_cardsList_api_should_fails_if_request_is_wrong() {
        let mockApiClient = MockCardsApiClient(hasError: true)
        let expectation = self.expectation(description: "Cards Api will success")
        guard let url = URL(string: MockApiConstants.ApiUrl.wrongMockApiUrl) else { XCTFail(); return }
        let urlRequest = URLRequest(url: url)
        mockApiClient.fetchCards(request: urlRequest) { (result) in
            switch result {
            case .failure(let error):
                XCTAssertNotNil(error)
                expectation.fulfill()
            case .success:
                XCTFail()
            }
        }
        waitForExpectations(timeout: 60, handler: nil)
    }
}
